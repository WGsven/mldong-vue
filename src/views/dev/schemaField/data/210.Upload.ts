export default {
  label: '附件上传',
  value: 'Upload',
  props: [
    {
      field: 'ext.api',
      label: '接口地址',
      component: 'Input',
      colProps: {
        xl: 12,
        xxl: 12,
      },
    },
  ],
};
