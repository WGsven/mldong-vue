/* eslint-disable */
import { handleIfShow, dynamicDisabled } from '/@/utils/action';
// 表单元数据配置
export const formSchemas = [
  {
    field: 'id',
    label: '主键',
    component: 'Input',
    colProps: {
      xl: 0,
      xxl: 0,
    },
  },
  {
    field: 'baseInfo',
    label: '基本信息',
    component: 'Divider',
    rules: [{ required: false, message: '请输入基本信息', trigger: 'blur' }],
    colProps: {
      xl: 24,
      xxl: 24,
    },
    componentProps: {
      dashed: false,
      plain: false,
      placeholder: "请输入基本信息",
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":1,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"Divider_dashed":false,"Divider_plain":false};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":1,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"Divider_dashed":false,"Divider_plain":false};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'name',
    label: '名称',
    component: 'Input',
    rules: [{ required: false, message: '请输入名称', trigger: 'blur' }],
    colProps: {
      xl: 24,
      xxl: 24,
    },
    componentProps: {
      placeholder: "请输入名称",
    },
    ifShow(e) {
      const ext = {"searchType":"LIKE","addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":1};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"searchType":"LIKE","addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":1,"editDisabled":0,"required":0,"search":1};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'inputTextArea',
    label: '多行文本',
    component: 'InputTextArea',
    rules: [{ required: false, message: '请输入多行文本', trigger: 'blur' }],
    colProps: {
      xl: 24,
      xxl: 24,
    },
    componentProps: {
      placeholder: "请输入多行文本",
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"showCount":true,"autoSize":{"minRows":3,"maxRows":5}};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"showCount":true,"autoSize":{"minRows":3,"maxRows":5}};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'autoComplete',
    label: '自动完成',
    component: 'ApiAutoComplete',
    rules: [{ required: false, message: '请输入自动完成', trigger: 'blur' }],
    colProps: {
      xl: 12,
      xxl: 12,
    },
    componentProps: {
      api: "/sys/user/select",
      immediate: true,
      placeholder: "请输入自动完成",
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiAutoComplete_api":"/sys/user/select","ApiAutoComplete_immediate":true,"span":12};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiAutoComplete_api":"/sys/user/select","ApiAutoComplete_immediate":true,"span":12};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'mSwitch',
    label: '开关',
    component: 'Switch',
    rules: [{ required: false, message: '请输入开关', trigger: 'blur', type: 'string' }],
    colProps: {
      xl: 12,
      xxl: 12,
    },
    componentProps: {
      dataType: "string",
      checkedValue: "是",
      unCheckedValue: "否",
      placeholder: "请输入开关",
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"Switch_dataType":"string","Switch_checkedValue":"是","Switch_unCheckedValue":"否","span":12};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"Switch_dataType":"string","Switch_checkedValue":"是","Switch_unCheckedValue":"否","span":12};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'divider',
    label: '分割线',
    component: 'Divider',
    rules: [{ required: false, message: '请输入分割线', trigger: 'blur' }],
    colProps: {
      xl: 24,
      xxl: 24,
    },
    componentProps: {
      dashed: false,
      orientation: "center",
      type: "horizontal",
      plain: false,
      placeholder: "请输入分割线",
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"Divider_dashed":false,"Divider_orientation":"center","Divider_type":"horizontal","Divider_plain":false};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"Divider_dashed":false,"Divider_orientation":"center","Divider_type":"horizontal","Divider_plain":false};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'apiSelect',
    label: '远程下拉',
    component: 'ApiSelect',
    rules: [{ required: false, message: '请输入远程下拉', trigger: 'blur' }],
    colProps: {
      xl: 24,
      xxl: 24,
    },
    componentProps: {
      immediate: true,
      api: "/sys/user/select",
      params: {"labelKey":"name1","valueKey":"id1"},
      placeholder: "请选择远程下拉",
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiSelect_immediate":true,"ApiSelect_api":"/sys/user/select","ApiSelect_params":{"labelKey":"name1","valueKey":"id1"}};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiSelect_immediate":true,"ApiSelect_api":"/sys/user/select","ApiSelect_params":{"labelKey":"name1","valueKey":"id1"}};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'dict',
    label: '字典',
    component: 'ApiDict',
    rules: [{ required: false, message: '请输入字典', trigger: 'blur', type: 'string' }],
    colProps: {
      xl: 24,
      xxl: 24,
    },
    componentProps: {
      code: "wf_process_type",
      dataType: "string",
      placeholder: "请选择字典",
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiDict_code":"wf_process_type","ApiDict_dataType":"string"};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiDict_code":"wf_process_type","ApiDict_dataType":"string"};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'apiTreeSelect',
    label: '远程树',
    component: 'ApiTreeSelect',
    rules: [{ required: false, message: '请输入远程树', trigger: 'blur' }],
    colProps: {
      xl: 24,
      xxl: 24,
    },
    componentProps: {
      api: "/sys/dept/tree",
      immediate: true,
      replaceFields: {"title":"name","value":"id","key":"id","children":"children"},
      placeholder: "请选择远程树",
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiTreeSelect_api":"/sys/dept/tree","ApiTreeSelect_immediate":true,"ApiTreeSelect_replaceFields":{"title":"name","value":"id","key":"id","children":"children"}};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiTreeSelect_api":"/sys/dept/tree","ApiTreeSelect_immediate":true,"ApiTreeSelect_replaceFields":{"title":"name","value":"id","key":"id","children":"children"}};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'apiRadioGroup',
    label: '远程单选组',
    component: 'ApiRadioGroup',
    rules: [{ required: false, message: '请输入远程单选组', trigger: 'blur' }],
    colProps: {
      xl: 12,
      xxl: 12,
    },
    componentProps: {
      api: "/sys/user/select",
      immediate: true,
      placeholder: "请输入远程单选组",
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiRadioGroup_api":"/sys/user/select","ApiRadioGroup_immediate":true,"span":12};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiRadioGroup_api":"/sys/user/select","ApiRadioGroup_immediate":true,"span":12};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'apiCheckboxGroup',
    label: '远程多选组',
    component: 'DemoCustomInput',
    rules: [{ required: false, message: '请输入远程多选组', trigger: 'blur' }],
    colProps: {
      xl: 12,
      xxl: 12,
    },
    componentProps: {
      params: "666",
      placeholder: "请输入远程多选组",
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiCheckboxGroup_api":"/sys/user/select","ApiCheckboxGroup_immediate":true,"CustomComponent_componentName":"DemoCustomInput","CustomComponent_params":"666","span":12};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiCheckboxGroup_api":"/sys/user/select","ApiCheckboxGroup_immediate":true,"CustomComponent_componentName":"DemoCustomInput","CustomComponent_params":"666","span":12};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'datePicker',
    label: '日期',
    component: 'DatePicker',
    rules: [{ required: false, message: '请输入日期', trigger: 'blur' }],
    colProps: {
      xl: 12,
      xxl: 12,
    },
    componentProps: {
      mode: "date",
      placeholder: "请输入日期",
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"DatePicker_mode":"date","span":12};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"DatePicker_mode":"date","span":12};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'rangePicker',
    label: '日期区间选择',
    component: 'RangePicker',
    rules: [{ required: false, message: '请输入日期区间选择', trigger: 'blur' }],
    colProps: {
      xl: 12,
      xxl: 12,
    },
    componentProps: {
      placeholder: ["开始日期","结束日期"],
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"span":12};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"span":12};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'apiCascader',
    label: '远程级联选择',
    component: 'ApiCascader',
    rules: [{ required: false, message: '请输入远程级联选择', trigger: 'blur' }],
    colProps: {
      xl: 24,
      xxl: 24,
    },
    componentProps: {
      api: "/sys/user/select",
      showSearch: true,
      immediate: true,
      placeholder: "请选择远程级联选择",
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiCascader_api":"/sys/user/select","ApiCascader_showSearch":true,"ApiCascader_immediate":true};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0,"ApiCascader_api":"/sys/user/select","ApiCascader_showSearch":true,"ApiCascader_immediate":true};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'upload',
    label: '文件上传',
    component: 'Upload',
    rules: [{ required: false, message: '请输入文件上传', trigger: 'blur' }],
    colProps: {
      xl: 24,
      xxl: 24,
    },
    componentProps: {
      placeholder: "请选择文件上传",
    },
    ifShow(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":0,"editHide":0,"listHide":0,"viewHide":0,"addDisabled":0,"editDisabled":0,"required":0,"search":0};
      return dynamicDisabled(e, ext);
    },
  },
  {
    field: 'updateTime',
    label: '修改时间',
    component: 'Input',
    rules: [{ required: false, message: '请输入修改时间', trigger: 'blur' }],
    colProps: {
      xl: 24,
      xxl: 24,
    },
    componentProps: {
      placeholder: "请输入修改时间",
    },
    ifShow(e) {
      const ext = {"addHide":1,"editHide":1,"listHide":0,"viewHide":1,"addDisabled":0,"editDisabled":0,"required":0,"search":0};
      return handleIfShow(e, ext);
    },
    dynamicDisabled(e) {
      const ext = {"addHide":1,"editHide":1,"listHide":0,"viewHide":1,"addDisabled":0,"editDisabled":0,"required":0,"search":0};
      return dynamicDisabled(e, ext);
    },
  },
];
// 详情元数据配置
export const detailSchemas = [
  {
    field: 'baseInfo',
    label: '基本信息',
    component: 'Divider',
    span: 24,
    componentProps: {
      dashed: false,
      plain: false,
      placeholder: "请输入基本信息",
    },
  },
  {
    field: 'name',
    label: '名称',
    component: 'Input',
    span: 24,
    componentProps: {
      placeholder: "请输入名称",
    },
  },
  {
    field: 'inputTextArea',
    label: '多行文本',
    component: 'InputTextArea',
    span: 24,
    componentProps: {
      placeholder: "请输入多行文本",
    },
  },
  {
    field: 'autoComplete',
    label: '自动完成',
    component: 'ApiAutoComplete',
    span: 12,
    componentProps: {
      api: "/sys/user/select",
      immediate: true,
      placeholder: "请输入自动完成",
    },
  },
  {
    field: 'mSwitch',
    label: '开关',
    component: 'Switch',
    span: 12,
    componentProps: {
      dataType: "string",
      checkedValue: "是",
      unCheckedValue: "否",
      placeholder: "请输入开关",
    },
  },
  {
    field: 'divider',
    label: '分割线',
    component: 'Divider',
    span: 24,
    componentProps: {
      dashed: false,
      orientation: "center",
      type: "horizontal",
      plain: false,
      placeholder: "请输入分割线",
    },
  },
  {
    field: 'apiSelect',
    label: '远程下拉',
    component: 'ApiSelect',
    span: 24,
    componentProps: {
      immediate: true,
      api: "/sys/user/select",
      params: {"labelKey":"name1","valueKey":"id1"},
      placeholder: "请选择远程下拉",
    },
  },
  {
    field: 'dict',
    label: '字典',
    component: 'ApiDict',
    span: 24,
    componentProps: {
      code: "wf_process_type",
      dataType: "string",
      placeholder: "请选择字典",
    },
  },
  {
    field: 'apiTreeSelect',
    label: '远程树',
    component: 'ApiTreeSelect',
    span: 24,
    componentProps: {
      api: "/sys/dept/tree",
      immediate: true,
      replaceFields: {"title":"name","value":"id","key":"id","children":"children"},
      placeholder: "请选择远程树",
    },
  },
  {
    field: 'apiRadioGroup',
    label: '远程单选组',
    component: 'ApiRadioGroup',
    span: 12,
    componentProps: {
      api: "/sys/user/select",
      immediate: true,
      placeholder: "请输入远程单选组",
    },
  },
  {
    field: 'apiCheckboxGroup',
    label: '远程多选组',
    component: 'DemoCustomInput',
    span: 12,
    componentProps: {
      params: "666",
      placeholder: "请输入远程多选组",
    },
  },
  {
    field: 'datePicker',
    label: '日期',
    component: 'DatePicker',
    span: 12,
    componentProps: {
      mode: "date",
      placeholder: "请输入日期",
    },
  },
  {
    field: 'rangePicker',
    label: '日期区间选择',
    component: 'RangePicker',
    span: 12,
    componentProps: {
      placeholder: ["开始日期","结束日期"],
    },
  },
  {
    field: 'apiCascader',
    label: '远程级联选择',
    component: 'ApiCascader',
    span: 24,
    componentProps: {
      api: "/sys/user/select",
      showSearch: true,
      immediate: true,
      placeholder: "请选择远程级联选择",
    },
  },
  {
    field: 'upload',
    label: '文件上传',
    component: 'Upload',
    span: 24,
    componentProps: {
      placeholder: "请选择文件上传",
    },
  },
];
// 表格列定义
export const tableColumns = [
    // 列定义
    {
      title: '名称',
      dataIndex: 'name',
      component: 'Input',
      componentProps: {
        placeholder: "请输入名称",
      },
    },
    {
      title: '多行文本',
      dataIndex: 'inputTextArea',
      component: 'InputTextArea',
      componentProps: {
        placeholder: "请输入多行文本",
      },
    },
    {
      title: '自动完成',
      dataIndex: 'autoComplete',
      component: 'ApiAutoComplete',
      componentProps: {
        api: "/sys/user/select",
        immediate: true,
        placeholder: "请输入自动完成",
      },
    },
    {
      title: '开关',
      dataIndex: 'mSwitch',
      component: 'Switch',
      componentProps: {
        dataType: "string",
        checkedValue: "是",
        unCheckedValue: "否",
        placeholder: "请输入开关",
      },
    },
    {
      title: '分割线',
      dataIndex: 'divider',
      component: 'Divider',
      componentProps: {
        dashed: false,
        orientation: "center",
        type: "horizontal",
        plain: false,
        placeholder: "请输入分割线",
      },
    },
    {
      title: '远程下拉',
      dataIndex: 'apiSelect',
      component: 'ApiSelect',
      componentProps: {
        immediate: true,
        api: "/sys/user/select",
        params: {"labelKey":"name1","valueKey":"id1"},
        placeholder: "请选择远程下拉",
      },
    },
    {
      title: '字典',
      dataIndex: 'dict',
      component: 'ApiDict',
      componentProps: {
        code: "wf_process_type",
        dataType: "string",
        placeholder: "请选择字典",
      },
    },
    {
      title: '远程树',
      dataIndex: 'apiTreeSelect',
      component: 'ApiTreeSelect',
      componentProps: {
        api: "/sys/dept/tree",
        immediate: true,
        replaceFields: {"title":"name","value":"id","key":"id","children":"children"},
        placeholder: "请选择远程树",
      },
    },
    {
      title: '远程单选组',
      dataIndex: 'apiRadioGroup',
      component: 'ApiRadioGroup',
      componentProps: {
        api: "/sys/user/select",
        immediate: true,
        placeholder: "请输入远程单选组",
      },
    },
    {
      title: '远程多选组',
      dataIndex: 'apiCheckboxGroup',
      component: 'DemoCustomInput',
      componentProps: {
        params: "666",
        placeholder: "请输入远程多选组",
      },
    },
    {
      title: '日期',
      dataIndex: 'datePicker',
      component: 'DatePicker',
      componentProps: {
        mode: "date",
        placeholder: "请输入日期",
      },
    },
    {
      title: '日期区间选择',
      dataIndex: 'rangePicker',
      component: 'RangePicker',
      componentProps: {
        placeholder: ["开始日期","结束日期"],
      },
    },
    {
      title: '远程级联选择',
      dataIndex: 'apiCascader',
      component: 'ApiCascader',
      componentProps: {
        api: "/sys/user/select",
        showSearch: true,
        immediate: true,
        placeholder: "请选择远程级联选择",
      },
    },
    {
      title: '文件上传',
      dataIndex: 'upload',
      component: 'Upload',
      componentProps: {
        placeholder: "请选择文件上传",
      },
    },
    {
      title: '修改时间',
      dataIndex: 'updateTime',
      component: 'Input',
      componentProps: {
        placeholder: "请输入修改时间",
      },
    },
    {
      // flag: 'ACTION',
      title: '操作',
      key: 'ACTION',
      dataIndex: 'ACTION',
      slots: { customRender: 'action' },
    },
  ];
// 搜索表单元数据
export const searchFormSchemas = [
    {
      field: 'm_LIKE_name',
      label: '名称',
      component: 'Input',
      colProps: {
        xl: 6,
        xxl: 6,
      },
      componentProps: {
        placeholder: "请输入名称",
      },
    },
  ];
/* eslint-disable */